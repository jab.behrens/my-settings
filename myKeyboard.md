# Ändern des Tastaturlayout

## Definition des Tastaturlayouts

Zuerst habe ich die Datei *usr/share/X11/xkb/symbols/de* ergänzt.

    // my own layout based on neo

    // Toggle CapsLock when pressed together with an other Shift key.
    partial modifier_keys
    xkb_symbols "altshift_both_capslock" {
      key <LALT> {
        type[Group1]="TWO_LEVEL",
        symbols[Group1] = [ Shift_L, Caps_Lock ]
      };
    };
    partial modifier_keys
    xkb_symbols "three_capslock" {
      include "shift(lshift_both_capslock)"
      include "shift(rshift_both_capslock)"
      include "de(altshift_both_capslock)"
    };
    // Right Ctrl functions as an Alt.
    partial modifier_keys
    xkb_symbols "rctrl_alt" {
        key <RCTL> { symbols[Group1]= [ Alt_L ] };
        modifier_map Mod1{ <RCTL> };
    };
    // Druck key functions as another right Ctrl.
    partial modifier_keys
    xkb_symbols "druck_rctrl" {
        replace key <PRSC> { [ Control_R, Control_R ] };
        modifier_map Control { Control_L, <PRSC> };
    };
    partial alphanumeric_keys modifier_keys keypad_keys
    xkb_symbols "jab" {

        include "de(neo_base)"

        name[Group1]= "German (jab, based on Neo 2)";

        include "de(three_capslock)"
        include "de(rctrl_alt)"
        include "de(druck_rctrl)"

        include "level3(caps_switch)"
        include "level3(bksl_switch)"
        include "level5(lsgt_switch_lock)"
        include "level5(ralt_switch_lock)"
    };

Mein Layout hat den Namen *jab* und nutzt das Layout *neo_base* aus der datei *de* als Grundlage.
Die Beschreibung ist *German (jab, based on Neo 2)*.
Dann bindet das Layout noch verschiedene Blöcke zur Definition der Modifier ein.
Die ersten drei sind aus der selben Datei (*de*) und von mir definiert.
Sie sorgen dafür, dass *Alt_L*, *Shift_L* und *Shift_R* alle Shift sind und zwei zusammen Caps_Lock aktivieren, *<RCTL>* zu Alt_L wird und *<PRSC>* zu Control_R wird.
Die anderen Blöcke stammen aus den Datein *level3* und *level5*.
Sie sorgen dafür, dass *Caps_Lock* und die *#-Taste* Level3-Modifier werden und *AltGr* und die *<>|-Taste* zu Level5-Modifier werden.

## Layoutlisten ergänzen

Dann müssen noch die vier Datein *evdev.xml*, *evdev.lst*, *base.xml* und *base.lst* im Verzeichnis *usr/share/X11/xkb/rules* ergänzt werden.
In den *.xml* Datein muss noch eine Variante in der *variantlist* hinzugefügt werden.

    <variant>
      <configItem>
         <name>jab</name>
         <description>German (my own layout)</description>
      </configItem>
     </variant>

In den *.lst* Datein wird noch folgende Zeile bei den Varianten ergänzt.

    jab             de: German (my own layout)

## Keyboardlayout aktivieren

Führt man in der Shell dann

    localectl list-x11-keymap-variants de

aus, so müsste *jab* vorhanden sein.
Für die aktuelle Sitzung lässt sich das Layout dann mit

    setxkbmap de jab

aktivieren. Für dauerhafte Benutzung muss in *etc/default/keyboard* die Option *XKBVARIANT* auf *jab* geändert werden.
Mit dem Befehl

    xcape -e "Shift_L=Return;Alt_L=Control_R|C;Control_R=Control_R|Shift_L|V;ISO_Level3_Shift=Escape"

kann man dann noch Aktionen auf die Modifier legen, wenn sie alleine gedrückt und nicht gehalten werden.

## Hilfreiche Links

- https://maximilian-schillinger.de/konfiguration-kou.html
- https://medium.com/@damko/a-simple-humble-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450
- https://github.com/alols/xcape
