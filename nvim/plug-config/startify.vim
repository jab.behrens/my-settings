let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]
"           \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
let g:startify_bookmarks = [
            \ { 'c': '~/.config/i3/config' },
            \ { 'i': '~/.config/nvim/init.vim' },
            \ { 'z': '~/.zshrc' },
            \ { 'd': '~/vimwiki/diary/diary.md'},
            \ { 'l': '~/vimwiki/links.md'},
            \ { 'i': '~/vimwiki/index.md'},
            \ ]
let g:ascii = [
          \'          JJJJJJJJJAABBBBBBBBBBBBBBBBB   ',
          \'          J::::::::::A::::::::::::::::B  ',
          \'          J:::::::::::A:::::BBBBBB:::::B ',
          \'          JJ:::::::::::A::::B     B:::::B',
          \'            J:::::J:::::A:::B     B:::::B',
          \'            J:::::JA:::::A::B     B:::::B',
          \'            J:::::J A:::::A:BBBBBB:::::B ',
          \'            J:::::J  A:::::A:::::::::BB  ',
          \'            J:::::J   A:::::ABBBBB:::::B ',
          \'JJJJJJJ     J:::::JAAAAA:::::A    B:::::B',
          \'J:::::J     J:::::J:::::::::::A   B:::::B',
          \'J::::::J   J::::::JAAAAAAA:::::A  B:::::B',
          \'J:::::::JJJ:::::::J       A:::::AB::::::B',
          \' JJ:::::::::::::JJ         A:::::A:::::B ',
          \'   JJ:::::::::JJ            A:::::A:::B  ',
          \'     JJJJJJJJJ               AAAAAAABB   '
          \]
" let g:ascii = [
"           \ '    _       _           _             _   _  __        ',
"           \ '   (_) __ _| |__    ___| |_ __ _ _ __| |_(_)/ _|_   _  ',
"           \ '   | |/ _` |  _ \  / __| __/ _` |  __| __| | |_| | | | ',
"           \ '   | | (_| | |_) | \__ \ || (_| | |  | |_| |  _| |_| | ',
"           \ '  _/ |\__,_|_.__/  |___/\__\__,_|_|   \__|_|_|  \__, | ',
"           \ ' |__/                                           |___/  ',
"           \ ''
"           \]
let g:startify_custom_header =
            \ 'startify#pad(g:ascii + startify#fortune#boxed())'
