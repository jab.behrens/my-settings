call plug#begin()
Plug 'joshdick/onedark.vim'                         " Colorscheme
Plug 'nvim-lualine/lualine.nvim'                    " statusline
Plug 'kyazdani42/nvim-web-devicons'                 " icons recommended by lualine
Plug 'mhinz/vim-startify'                           " startscreen
Plug 'vimwiki/vimwiki'                              " vimwiki
Plug 'tpope/vim-commentary'                         " comment visual with gc
Plug 'tpope/vim-surround'                           " change surrounding ' with cs'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " Fuzzyfinder
Plug 'junegunn/fzf.vim'
Plug 'lotabout/skim', { 'dir': '~/.skim', 'do': './install' }
Plug 'ibhagwan/fzf-lua'
Plug 'tpope/vim-fugitive'                           " git-integration
Plug 'norcalli/nvim-colorizer.lua'                  " color-preview
Plug 'SirVer/ultisnips'                             " snippet Engine :Snippets
" Plug 'airblade/vim-gitgutter'                     " git-annotations
Plug 'ervandew/supertab'                            " Tab completion
" coc/ycm or deoplete
Plug 'iamcco/markdown-preview.nvim', {'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug', 'vimwiki']}
Plug 'ryanoasis/vim-devicons'                       " icons from nerdfont
Plug 'kchmck/vim-coffee-script'                     " coffee-script support
Plug 'unblevable/quick-scope'                       " marks individual chars in row on pressing f
Plug 'neoclide/coc.nvim', {'branch': 'release'}     " Conquerer of Completion
Plug 'szw/vim-g'                                    " Google-Search
call plug#end() " :PlugInstall

" I use space as my leader
" but to avoid conflicts with plugins mapleader is set to ,
let mapleader = ","

" automatically reload my vimrc when saved
autocmd! bufwritepost $MYVIMRC source %
" load config file in vertical split
nnoremap <space>v :vsp $MYVIMRC<cr> 

python3_host_prog="/usr/bin/python3"

" set colorscheme (it looks like Atom)
colorscheme onedark

" Lualine (Statusline)
" --------------------
source $HOME/.config/nvim/plug-config/statusline.vim

set colorcolumn=80        " visual hint where you reach 80 characters
set number                " numbering on left side
set relativenumber        " only current line with absolute line number
set clipboard=unnamedplus " Copy Paste between vim and everything else
set expandtab             " convert tab to spaces
set tabstop=4
set shiftwidth=4
set autowriteall          " write on any leaving on buffer
set ignorecase            " while searching undo with \C in search
set smartcase             " case-sensitive search if it contains upper-case characters
set nobackup              " no backup file
set noswapfile            " instead of swap use git
set pumheight=10          " height of pop up menu
set list                  " show trailing spaces and tabs  see listchars
set smartindent           " makes indenting smart
set autochdir             " your working dir = file dir

set cursorline    " highlight current line
set showtabline=2 " always show tabline at the top
set scrolloff=3   " keep more context when scrolling off the end of a buffer
set sidescrolloff=8
set autoread      " if a file is changed outside of vim, automatically reload without asking
set title         " Show file title at the top of terminal window
set splitbelow    " Horizontal splits will automatically be below
set splitright    " Vertical splits will automatically be to the right
set incsearch     " set incremental search, like modern browsers
" set wildmenu      " enhanced command line completion
" set wildmode=full "list","longest" " complete files like a shell
set hidden        " When leaving buffer, hide it instead of closing it
" set nowrap        " no line breaks
"set foldmethod=marker
"set foldopen+=jump

" change with of splited window
nnoremap <space>+ :vertical resize +5<CR>
nnoremap <space>- :vertical resize -5<CR>

" buffer-management
" -----------------
" Delete all hidden buffers
function! DeleteHiddenBuffers()
  let tpbl=[]
  call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
  for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
    silent execute 'bwipeout' buf
  endfor
endfunction
nnoremap dB :call DeleteHiddenBuffers()<cr>
" delete current buffer
nnoremap db :bd<cr>
" switch to last buffer
nnoremap <space><space> :b#<cr>
" switch to next buffer
nnoremap gb :bn<cr>
" switch to previous buffer
nnoremap gB :bp<cr>
" NR + gb jumps to buffer with NR
let c = 1
while c <= 99
  execute "nnoremap " . c . "gb :" . c . "b\<CR>"
  let c += 1
endwhile

" Swap 0 and ˆ in order to make easier to move to the first non-whitespace
" character
nnoremap 0 ^
nnoremap ^ 0
" automatically rebalance windows on resize
autocmd VimResized * :wincmd =

" ctrl+s to save
nnoremap <c-s> :w<cr>
inoremap <c-s> <esc>:w<cr>
" ctrl+q to quit
nnoremap <c-q> :wq<cr>

" curser movement (I'm using neo2 as my keyboardlayout)
nnoremap i <left>
nnoremap a <down>
nnoremap e <right>
nnoremap l <up>
vnoremap i <left>
vnoremap a <down>
vnoremap e <right>
vnoremap l <up>
" move around splits with <c-iael>
nnoremap <c-i> <c-w>h
nnoremap <c-a> <c-w>j
nnoremap <c-e> <c-w>l
nnoremap <c-l> <c-w>k

" remove highlighted searchresults
nnoremap <silent><space>n :nohl<cr>

" close all other splits
nnoremap <space>o :only<cr>

" rename current file
function! RenameFile()
    let old_name = expand('%')
    let new_name = input('New file name: ', expand('%'), 'file')
    if new_name != '' && new_name != old_name
        exec ':saveas ' . new_name
        exec ':silent !rm ' . old_name
        redraw!
    endif
endfunction
nnoremap <space>r :call RenameFile()<cr>

" for markdown preview
nmap <space>ps <Plug>MarkdownPreview
nmap <space>pe <Plug>MarkdownPreviewStop
let g:mkdp_auto_close = 0 " don't close preview automatically
" let g:mkdp_command_for_global = 0 " markdown_preview for all files
let g:mkdp_filetypes = ['litcoffee', 'markdown']
"
" tm => toggle the markdown preview
"     let g:markdown_preview_on = 0
"     au! BufWinEnter *.md,*.markdown,*.mdown let g:markdown_preview_on = g:markdown_preview_auto || g:markdown_preview_on  
"     au! BufWinLeave *.md,*.markdown,*.mdown let g:markdown_preview_on = !g:markdown_preview_auto && g:markdown_preview_on  
"     nmap tm @=(g:markdown_preview_on ? ':Stop' : ':Start')<CR>MarkdownPreview<CR>:let g:markdown_preview_on = 1 - g:markdown_preview_on<CR>
"
"autocmd BufNewFile,BufReadPost *.md setfiletype markdown

" TODO <space>m bei markdown default: :pandoc -o filename.pdf file or bash
" script or custom command if mentioned in header

" (literate) coffee-script
au BufNewFile,Bufread *.coffee,*.litcoffee :map <buffer> <space>r :w<CR>:CoffeeRun<CR>
au BufNewFile,Bufread *.coffee,*.litcoffee :map <buffer> <space>m :w<cr>:make --bare<cr>:"Make Coffee-script"<cr>

" for my linkfile
" :autocmd BufEnter *links.md call LinkInsert()
fun! TimeAtEnd()
    normal! Go
    let text = strftime("%Y-%m-%d %H:%M:%S")
    exe "normal! a" . text . " \<Esc>"
endfun

fun! LinkInsert()
    execute system("/home/jab/bin/xdotool.sh")
    call TimeAtEnd()
    normal! p
    " normal! :wq
endfun

" vimwiki
" -------
set nocompatible
filetype plugin on
syntax on
let g:vimwiki_list=[{'syntax': 'markdown', 'ext': '.md'}]

" fzf.vim
" -------
" I use fzf-lua instead
" let g:fzf_layout = { 'down': '40%' }
" let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
" nnoremap <space>h :History<cr>
" nnoremap <space>f :Files<cr>
" cnoreabbrev fgf :GFiles?<cr>
" nnoremap <space>g :Rg<space>
" nnoremap <space>ll :Lines
" nnoremap <space>lb :BLines<cr>
" cnoreabbrev fh :History:<cr>
" cnoreabbrev fsh :History/<cr>
" cnoreabbrev fc :Commits<cr>
" nnoremap <leader>m :Maps<cr>
" nnoremap <space>af :FZF ~<cr>

" fzf-lua
" -------
lua << END
require'fzf-lua'.setup {
    fzf_bin = '/home/jab/.fzf/bin/fzf',
}
END
nnoremap h <cmd>lua require('fzf-lua').oldfiles()<CR>
nnoremap <space>h <cmd>lua require('fzf-lua').oldfiles()<CR>
nnoremap <space>f <cmd>lua require('fzf-lua').files()<CR>
nnoremap <space>ll <cmd>lua require('fzf-lua').lines()<CR>
nnoremap <space>lb <cmd>lua require('fzf-lua').blines()<CR>
nnoremap <space>lt <cmd>lua require('fzf-lua').tabs()<CR>
nnoremap <space>g <cmd>lua require('fzf-lua').grep()<CR>
nnoremap <space>sw <cmd>lua require('fzf-lua').grep_cword()<CR>
nnoremap <space>sv <cmd>lua require('fzf-lua').grep_visual()<CR>
nnoremap <space>m <cmd>lua require('fzf-lua').keymaps()<CR>
nnoremap <space>sc <cmd>lua require('fzf-lua').changes()<CR>
nnoremap <space>sb <cmd>lua require('fzf-lua').builtin()<CR>

" jb
" --
source /home/jab/.config/nvim/jb.vim
let g:Jb_Linkfname = '/home/jab/vimwiki/links.md'
let g:Jb_browser = 'brave'
"! open Browser with link in cursorline
nnoremap <space>b   :call JbBrows('')<cr>
"! find all lines with todos in edited files
nnoremap <space>jt  :call JbFzEdit('\* 20')<cr>
"! Fuzzy grep cursorword in edited files
nnoremap <space>e   :call JbFzEdit('')<cr>
"! Fuzzy list all markdown links containing cursorword
nnoremap <space>l   :call JbFzLink('')<cr> 
"! Fuzzy list all edited Files
nnoremap <space>..  :call JbFzEdit('.*')<cr>
"! open command for input fuzzy search on edited files
nnoremap <space>.<space> :Je .#

" Folding
" -------
" fold every paragraph = show first line after empty line
noremap z<space> :set foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'\\S'?'<1':1<CR>:set fdm=expr<CR><CR>zM
" fold all lines not containing search 
nnoremap z/ :set foldmethod=expr foldlevel=0 foldcolumn=2<CR>:exe ":set foldexpr=(getline(v:lnum)!~'". substitute(escape(@/, '\/.*$^~[]'), "[ \t\n]\\+", "\\\\_s\\\\+", "g")."')" <CR>zM
" open all folds
nnoremap zn :set nofen<CR>zR

" ultisnips
" ---------
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical" " :UltiSnipsEdit will split window

" colorizer.lua
" -------------
" changes background too
set termguicolors
lua require 'colorizer'.setup()

" quickscope
" ----------
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
highlight QuickScopePrimary guifg='#00C7DF' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#afff5f' gui=underline ctermfg=81 cterm=underline
let g:qs_max_chars=150

" Conquer of Completion
source $HOME/.config/nvim/plug-config/coc.vim

" startify
source $HOME/.config/nvim/plug-config/startify.vim

" inspiration
" -----------
" chrisatmachine.com
" https://github.com/garybernhardt/dotfiles/blob/main/.vimrc
" https://github.com/WaylonWalker/devtainer/blob/main/nvim/.config/nvim/keymap.vim
" https://github.com/ThePrimeagen/.dotfiles/blob/master/nvim/.config/nvim/init.vim
"
" https://github.com/nicknisi/dotfiles/blob/main/config/nvim/init.lua
" https://github.com/zenbro/dotfiles/blob/master/.nvimrc#L151-L187
"
" https://github.com/euclio/vimrc/blob/master/vimrc
" https://github.com/tpope/tpope/blob/master/.vimrc
" https://github.com/lukas-reineke/dotfiles/blob/master/vim/lua/mappings.lua
" https://github.com/junegunn/dotfiles/blob/master/vimrc

" vnoremap $1 <esc>`>a)<esc>`<i(<esc> " Surround the visual selection in parenthesis/brackets/etc.:
" inoremap $1 ()<esc>i " Quickly insert parenthesis/brackets/etc.:

" nnoremap <space>1 1gt " Switch between tabs
" nnoremap <space>2 2gt
